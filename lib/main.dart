import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'features/todo_app/presentation/providers/web_service_api_provider.dart';
import 'features/todo_app/presentation/routes/routes.dart';

void main() => runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => WebServiceApiProvider())
        ],
        child: MyApp(),
      ),
    );

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // brightness: Brightness.dark,
        primaryColor: Color(0xFFF96060),
        textTheme: TextTheme(
          headline5: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 18.0, fontStyle: FontStyle.normal),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      // initialRoute: 'todoScreenPage',
      initialRoute: 'todoAppPage',
      routes: appRoutes,
    );
  }
}
