import 'package:floor/floor.dart';
import 'package:flutter_todo_app/features/todo_app/domain/entities/user_task_entity.dart';

@Entity(tableName: 'usertaskmodel')
class DbUserTaskEntityModel extends UserTasksDomainEntity {
  @PrimaryKey(autoGenerate: true)
  final int? id;
  final String? usertask;
  final List<dynamic>? data;

  DbUserTaskEntityModel({
    this.id,
    this.usertask,
    this.data,
  }) : super(data: data);
}
