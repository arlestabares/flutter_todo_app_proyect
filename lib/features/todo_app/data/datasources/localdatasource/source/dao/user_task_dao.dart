import 'package:floor/floor.dart';
import 'package:flutter_todo_app/features/todo_app/data/models/db_user_task_model.dart';

@dao
abstract class UseTaskDao {
  @Insert(onConflict: OnConflictStrategy.ignore)
  Future<void> insertUserTask(DbUserTaskEntityModel userTask);

  @Insert(onConflict: OnConflictStrategy.ignore)
  Future<void> insertRandomCatPhrasesList(
      List<DbUserTaskEntityModel> userTaskModel);

  @Update(onConflict: OnConflictStrategy.replace)
  Future<void> updateUseTask(DbUserTaskEntityModel usertask);

  @Query('SELECT * FROM usertaskmodel WHERE name LIKE :name')
  Stream<List<DbUserTaskEntityModel>> findUserTaskByNameLikeAsStream(
      String name);

  @Query('SELECT * FROM  usertaskmodel')
  Stream<List<DbUserTaskEntityModel>> findAllUserTaskAsStream();
  //
  @Query('SELECT * FROM usertaskmodel')
  Future<List<DbUserTaskEntityModel>> findAllUserTask();

  @Query('SELECT * FROM usertaskmodel WHERE  id = :id')
  Future<DbUserTaskEntityModel> findUserTaskById(int id);

  @Query('DELETE FROM usertaskmodel')
  Future<void> deleteAllUserTask();
  //
  @Query('DELETE FROM usertaskmodel WHERE id = :id')
  Future<void> deleteUserTaskById(int id);
}
