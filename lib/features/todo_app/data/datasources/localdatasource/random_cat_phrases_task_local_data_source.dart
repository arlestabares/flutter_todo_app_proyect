import 'package:flutter_todo_app/features/todo_app/data/models/db_user_task_model.dart';

import 'source/dao/user_task_dao.dart';

abstract class IRandomCatPhrasesAndTasksLocalDataSource {
  Future<void> insertUserTask(DbUserTaskEntityModel userTask);
  Future<void> insertRandomCatPhrasesList(List<DbUserTaskEntityModel> userlist);

  Future<DbUserTaskEntityModel> findUserTaskById(int id);
  Future<void> deleteUserTaskById(int id);
  Future<void> deleteAllUserTask();
  Future<void> updateUseTask(DbUserTaskEntityModel userTaskEntityModel);

  Stream<List<DbUserTaskEntityModel>> findUserTaskByNameLikeAsStream(
      String name);
  Stream<List<DbUserTaskEntityModel>> findAllUserTaskAsStream();
}

class RandomCatPhrasesTaskLocalDataSourceImpl
    implements IRandomCatPhrasesAndTasksLocalDataSource {
  final UseTaskDao useTaskDao;

  RandomCatPhrasesTaskLocalDataSourceImpl(this.useTaskDao);
  //
  //
  @override
  Future<void> deleteAllUserTask() {
    return useTaskDao.deleteAllUserTask();
  }

  @override
  Future<void> deleteUserTaskById(int id) {
    return useTaskDao.deleteUserTaskById(id);
  }

  @override
  Stream<List<DbUserTaskEntityModel>> findAllUserTaskAsStream() {
    return useTaskDao.findAllUserTaskAsStream();
  }

  @override
  Future<DbUserTaskEntityModel> findUserTaskById(int id) {
    return useTaskDao.findUserTaskById(id);
  }

  @override
  Stream<List<DbUserTaskEntityModel>> findUserTaskByNameLikeAsStream(
      String name) {
    return useTaskDao.findUserTaskByNameLikeAsStream(name);
  }

  @override
  Future<void> insertRandomCatPhrasesList(
      List<DbUserTaskEntityModel> userlist) {
    return useTaskDao.insertRandomCatPhrasesList(userlist);
  }

  @override
  Future<void> insertUserTask(DbUserTaskEntityModel userTask) {
    return useTaskDao.insertUserTask(userTask);
  }

  @override
  Future<void> updateUseTask(DbUserTaskEntityModel dbUserTaskEntityModel) {
    return useTaskDao.updateUseTask(dbUserTaskEntityModel);
  }
}
