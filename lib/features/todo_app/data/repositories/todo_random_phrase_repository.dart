import 'package:dartz/dartz.dart';
import 'package:flutter_todo_app/core/errors/exception.dart';
import 'package:flutter_todo_app/core/errors/failure.dart';
import 'package:flutter_todo_app/core/network/network_info.dart';
import 'package:flutter_todo_app/features/todo_app/data/converters/converters.dart';
import 'package:flutter_todo_app/features/todo_app/data/datasources/localdatasource/random_cat_phrases_task_local_data_source.dart';
import 'package:flutter_todo_app/features/todo_app/data/datasources/remotedatasource/random_phrase_remote_data_source.dart';
import 'package:flutter_todo_app/features/todo_app/data/models/db_user_task_model.dart';
import 'package:flutter_todo_app/features/todo_app/domain/entities/user_task_entity.dart';
import 'package:flutter_todo_app/features/todo_app/domain/repositories/random_phrases_domain_repository.dart';

class RandomCatPhraseAndTaskRepositoryImpl
    implements IRandomCatPhraseAndTaskDomainRepository {
  final IRandomPhrasesRemoteDatasource iRemoteDatasource;
  final IRandomCatPhrasesAndTasksLocalDataSource iLocalDataSource;
  final INetworkInfo iNetworkInfo;
  final Converters converters;

  RandomCatPhraseAndTaskRepositoryImpl({
    required this.iRemoteDatasource,
    required this.iLocalDataSource,
    required this.iNetworkInfo,
    required this.converters,
  });

  @override
  Future<Either<IFailure, UserTasksDomainEntity>> getRandomCatPhraseList(
      int limit) async {
    if (await iNetworkInfo.isConnect) {
      try {
        final remoteRandomPhrase = await iRemoteDatasource
            .getRandomCatPhrasesRemoteList(limit)
            .then((value) => converters.userDToToDomainEntity(value));

        return Right(remoteRandomPhrase);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(CacheFailure());
    }
  }

  @override
  Future<UserTasksDomainEntity> getUserTaskById(int userId) {
    return iLocalDataSource.findUserTaskById(userId);
  }

  @override
  Stream<List<UserTasksDomainEntity>> findAllUserTaskAsStream() {
    return iLocalDataSource.findAllUserTaskAsStream();
  }

  @override
  Stream<List<UserTasksDomainEntity>> findUserTaskByNameLikeAsStream(
      String name) {
    return iLocalDataSource.findUserTaskByNameLikeAsStream(name);
  }

  @override
  Future<void> insertUserTask(DbUserTaskEntityModel usertask) {
    return iLocalDataSource.insertUserTask(usertask);
  }

  @override
  Future<void> insertUserTaskList(List<DbUserTaskEntityModel> userlist) {
    return iLocalDataSource.insertRandomCatPhrasesList(userlist);
  }

  @override
  Future<void> deleteAllUserTask() {
    return iLocalDataSource.deleteAllUserTask();
  }

  @override
  Future<void> deleteUserTask(int id) {
    return iLocalDataSource.deleteUserTaskById(id);
  }

  @override
  Future<void> updateUseTask(DbUserTaskEntityModel userTasksDomainEntity) {
    return iLocalDataSource.updateUseTask(userTasksDomainEntity);
  }
}
