import 'dart:convert';

// DatumDto datumDtoFromJson(String str) => DatumDto.fromJson(json.decode(str));
// String datumDtoToJson(DatumDto data) => json.encode(data.toJson());

List<DatumDto> datumDtoFromJson(String str) =>
    List<DatumDto>.from(json.decode(str).map((x) => DatumDto.fromJson(x)));

String datumDtoToJson(List<DatumDto> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DatumDto {
  DatumDto({
    this.fact,
    this.length,
  });

  String? fact;
  int? length;

  factory DatumDto.fromJson(Map<String, dynamic> json) => DatumDto(
        fact: json["fact"],
        length: json["length"],
      );

  Map<String, dynamic> toJson() => {
        "fact": fact,
        "length": length,
      };
}
