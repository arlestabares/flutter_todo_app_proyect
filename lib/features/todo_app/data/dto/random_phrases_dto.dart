import 'dart:convert';

RandomCatPhrasesDTO randomCatPhrasesFromJson(String str) =>
    RandomCatPhrasesDTO.fromJson(json.decode(str).toList());

String randomCatPhrasesToJson(RandomCatPhrasesDTO data) =>
    json.encode(data.toJson());

class RandomCatPhrasesDTO {
  RandomCatPhrasesDTO({
    this.data,
  });

  final List<Datum>? data;

  factory RandomCatPhrasesDTO.fromJson(Map<String, dynamic> json) =>
      RandomCatPhrasesDTO(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

// ignore: must_be_immutable
class Datum {
  Datum({this.fact});

  String? fact;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        fact: json["fact"],
      );

  Map<String, dynamic> toJson() => {
        "fact": fact,
      };
}
