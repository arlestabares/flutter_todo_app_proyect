import 'package:flutter_todo_app/features/todo_app/data/dto/random_phrases_dto.dart';
import 'package:flutter_todo_app/features/todo_app/data/models/db_user_task_model.dart';
import 'package:flutter_todo_app/features/todo_app/domain/entities/user_task_entity.dart';

class Converters {
  UserTasksDomainEntity userDToToDomainEntity(RandomCatPhrasesDTO userDto) {
    return UserTasksDomainEntity();
  }

  UserTasksDomainEntity userModelToDomainEntity(
      DbUserTaskEntityModel userModel) {
    return UserTasksDomainEntity();
  }
}
