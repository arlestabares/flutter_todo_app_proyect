import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_todo_app/core/errors/failure.dart';
import 'package:flutter_todo_app/core/usescases/random_cat_phrases_usecase.dart';
import 'package:flutter_todo_app/features/todo_app/domain/entities/user_task_entity.dart';
import 'package:flutter_todo_app/features/todo_app/domain/repositories/random_phrases_domain_repository.dart';

class GetRandomCatPhraseUseCase
    implements IRandomCatPhrasesUseCase<UserTasksDomainEntity?, Params> {
  final IRandomCatPhraseAndTaskDomainRepository iDomainRepository;

  GetRandomCatPhraseUseCase(this.iDomainRepository);
  @override
  Future<Either<IFailure, UserTasksDomainEntity?>> call(Params params) async {
    return await iDomainRepository.getRandomCatPhraseList(params.limit);
  }
}

class Params extends Equatable {
  final int limit;

  Params({required this.limit});
  @override
  List<Object?> get props => [limit];
}
