import 'package:equatable/equatable.dart';
import 'package:flutter_todo_app/core/usescases/user_task_usecase.dart';
import 'package:flutter_todo_app/features/todo_app/domain/entities/user_task_entity.dart';
import 'package:flutter_todo_app/features/todo_app/domain/repositories/random_phrases_domain_repository.dart';

class DeleteUserTask
    implements IUserTaskUseCase<UserTasksDomainEntity, Params> {
  final IRandomCatPhraseAndTaskDomainRepository iDomainRepository;

  DeleteUserTask(this.iDomainRepository);
  @override
  Future<void> call(Params params) {
    return iDomainRepository.deleteUserTask(params.id);
  }
}

class Params extends Equatable {
  final int id;

  Params(this.id);
  @override
  List<Object?> get props => [id];
}
