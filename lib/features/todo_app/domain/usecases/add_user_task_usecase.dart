import 'package:equatable/equatable.dart';
import 'package:flutter_todo_app/core/usescases/user_task_usecase.dart';
import 'package:flutter_todo_app/features/todo_app/data/models/db_user_task_model.dart';
import 'package:flutter_todo_app/features/todo_app/domain/entities/user_task_entity.dart';
import 'package:flutter_todo_app/features/todo_app/domain/repositories/random_phrases_domain_repository.dart';

class AddUserTaskUseCase
    implements IUserTaskUseCase<UserTasksDomainEntity, Params> {
  final IRandomCatPhraseAndTaskDomainRepository iDomainRepository;

  AddUserTaskUseCase(this.iDomainRepository);
  @override
  Future<void> call(Params params) async {
    return await iDomainRepository.insertUserTask(params.userTasksDomainEntity);
  }
}

class Params extends Equatable {
  final DbUserTaskEntityModel userTasksDomainEntity;

  Params(this.userTasksDomainEntity);
  @override
  List<Object?> get props => [];
}
