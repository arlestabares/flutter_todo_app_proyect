import 'package:equatable/equatable.dart';
import 'package:flutter_todo_app/core/usescases/search_user_task_usecase.dart';
import 'package:flutter_todo_app/features/todo_app/domain/entities/user_task_entity.dart';
import 'package:flutter_todo_app/features/todo_app/domain/repositories/random_phrases_domain_repository.dart';

class SearchUserTaskUseCase
    implements ISearchUserTaskUseCase<UserTasksDomainEntity, Params> {
  //
  final IRandomCatPhraseAndTaskDomainRepository iDomainRepository;

  SearchUserTaskUseCase(this.iDomainRepository);

  //
  @override
  Stream<List<UserTasksDomainEntity>> call(Params params) {
    return iDomainRepository.findUserTaskByNameLikeAsStream(params.name);
  }
}

class Params extends Equatable {
  final String name;

  Params(this.name);

  @override
  List<Object?> get props => [name];
}
