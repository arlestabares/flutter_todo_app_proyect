import 'package:equatable/equatable.dart';
import 'package:flutter_todo_app/core/usescases/user_task_usecase.dart';
import 'package:flutter_todo_app/features/todo_app/data/models/db_user_task_model.dart';
import 'package:flutter_todo_app/features/todo_app/domain/entities/user_task_entity.dart';
import 'package:flutter_todo_app/features/todo_app/domain/repositories/random_phrases_domain_repository.dart';

class UpdateUserTaskUseCase
    implements IUserTaskUseCase<UserTasksDomainEntity, Params> {
  final IRandomCatPhraseAndTaskDomainRepository iDomainRepository;

  UpdateUserTaskUseCase(this.iDomainRepository);

  //
  @override
  Future<void> call(Params params) {
    return iDomainRepository.updateUseTask(params.userTasksEntity);
  }
}

class Params extends Equatable {
  final DbUserTaskEntityModel userTasksEntity;

  Params(this.userTasksEntity);

  @override
  List<Object?> get props => [userTasksEntity];
}
