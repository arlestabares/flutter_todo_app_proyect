import 'package:dartz/dartz.dart';
import 'package:flutter_todo_app/core/errors/failure.dart';
import 'package:flutter_todo_app/features/todo_app/data/models/db_user_task_model.dart';
import 'package:flutter_todo_app/features/todo_app/domain/entities/user_task_entity.dart';

abstract class IRandomCatPhraseAndTaskDomainRepository {
  Future<Either<IFailure, UserTasksDomainEntity?>> getRandomCatPhraseList(
      int limit);

  Future<void> insertUserTask(DbUserTaskEntityModel usertask);
  Future<void> insertUserTaskList(List<DbUserTaskEntityModel> userlist);
  //
  Stream<List<UserTasksDomainEntity>> findAllUserTaskAsStream();
  Future<UserTasksDomainEntity> getUserTaskById(int userId);
  Stream<List<UserTasksDomainEntity>> findUserTaskByNameLikeAsStream(
      String name);

  Future<void> deleteUserTask(int id);
  Future<void> deleteAllUserTask();
  Future<void> updateUseTask(DbUserTaskEntityModel userTasksDomainEntity);
}
