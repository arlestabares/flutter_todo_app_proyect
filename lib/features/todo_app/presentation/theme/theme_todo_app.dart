import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/constants/constanst.dart';

final myThemeResponsive = ThemeData(
  primaryColor: kPrimaryColor,
  scaffoldBackgroundColor: kBgColor,
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: TextButton.styleFrom(backgroundColor: kPrimaryColor),
  ),
  textTheme: TextTheme(
      bodyText1: TextStyle(color: kBodyTextColor),
      bodyText2: TextStyle(color: kBodyTextColor),
      headline5: TextStyle(color: kBodyTextColor)),
);
