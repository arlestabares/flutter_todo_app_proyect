part of 'app_to_do_bloc.dart';

abstract class AppToDoEvent extends Equatable {
  const AppToDoEvent();
  @override
  List<Object?> get props => [];
}

class GetRandomCatPhraseUseCaseEvent extends AppToDoEvent {
  final String numberString;

  GetRandomCatPhraseUseCaseEvent(this.numberString);

  @override
  List<Object?> get props => [numberString];
}

class AddUseTaskUseCaseEvent extends AppToDoEvent {}

class UpdateTaskUseCaseEvent extends AppToDoEvent {}

class SearchUserTask extends AppToDoEvent {}

class DeleteUserTask extends AppToDoEvent {}
