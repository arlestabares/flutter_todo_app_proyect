part of 'app_to_do_bloc.dart';

abstract class AppToDoState extends Equatable {
  const AppToDoState();
  @override
  List<Object> get props => [];
}

class Empty extends AppToDoState {}

class Loading extends AppToDoState {}

class Loaded extends AppToDoState {
  final UserTasksDomainEntity userTasksEntity;

  Loaded(this.userTasksEntity);
}

class Error extends AppToDoState {
  final String message;

  Error({required this.message}) : super();

  @override
  List<Object> get props => [message];
}
