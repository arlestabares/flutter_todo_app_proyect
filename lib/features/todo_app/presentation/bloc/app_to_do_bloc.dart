import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_todo_app/features/todo_app/domain/entities/user_task_entity.dart';
part 'app_to_do_event.dart';
part 'app_to_do_state.dart';

class AppToDoBloc extends Bloc<AppToDoEvent, AppToDoState> {
  AppToDoBloc() : super(Empty());
  @override
  Stream<AppToDoState> mapEventToState(
    AppToDoEvent event,
  ) async* {}
}
