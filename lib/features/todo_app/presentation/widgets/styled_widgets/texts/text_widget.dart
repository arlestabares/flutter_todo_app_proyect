import 'package:flutter/material.dart';

class TextWidget extends StatelessWidget {
  final String? text;
  final TextStyle? style;
  final int? maxLines;

  const TextWidget({required this.text, this.style, this.maxLines});
  @override
  Widget build(BuildContext context) {
    return Text(
      text!,
      style: style,
      maxLines: maxLines,
    );
  }
}
