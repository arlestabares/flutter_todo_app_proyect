import 'package:flutter/material.dart';

class AlertDialogWidgetAndroid extends StatelessWidget {
  final Widget? title;
  final Widget? content;
  final List<Widget>? actions;
  final String? textOfButton;
  final TextStyle? textStyle;
  final VoidCallback? onPressed;

  AlertDialogWidgetAndroid({
    required this.title,
    required this.content,
    this.actions,
    this.onPressed,
    this.textOfButton,
    this.textStyle,
  });
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: title,
      content: content,
      actions: actions,
    );
  }
}
