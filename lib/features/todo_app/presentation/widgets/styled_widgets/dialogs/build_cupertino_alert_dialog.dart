import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlertDialogWidgetCupertino extends StatelessWidget {
  final Widget? title;
  final Widget? content;
  final List<Widget>? actions;
  final String? textButton;
  final TextStyle? textStyle;
  final VoidCallback? onPressed;

  AlertDialogWidgetCupertino({
    required this.title,
    required this.content,
    required this.actions,
    this.textButton,
    this.textStyle,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: title,
      content: content,
      actions: actions!,
    );
  }
}
