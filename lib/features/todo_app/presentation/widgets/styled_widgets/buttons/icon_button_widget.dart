import 'package:flutter/material.dart';

class IconButtonWidget extends StatelessWidget {
  final Widget? icon;
  final double? iconSize;
  final VoidCallback? onPressed;

  IconButtonWidget({
    required this.icon,
    required this.onPressed,
    this.iconSize,
  });
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: icon!,
      iconSize: iconSize ?? 30.0,
      onPressed: onPressed!,
    );
  }
}
