import 'package:flutter/material.dart';

class TextButtonWidget extends StatelessWidget {
  final Widget? child;
  final ButtonStyle? style;
  final VoidCallback? onPressed;

  const TextButtonWidget({
    required this.child,
    this.style,
    required this.onPressed,
  });
  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: child!,
      style: style,
      onPressed: onPressed,
    );
  }
}
