import 'package:flutter/material.dart';

class FloatinActionButtonWidget extends StatelessWidget {
  final String? text;
  final VoidCallback? onPressed;
  final Color? backgroundColor;

  FloatinActionButtonWidget({this.text, this.onPressed, this.backgroundColor});
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.extended(
      label: Text(text!),
      onPressed: onPressed,
      backgroundColor: backgroundColor,
    );
  }
}
