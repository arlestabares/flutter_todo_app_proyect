import 'package:equatable/equatable.dart';
import 'package:flutter_todo_app/features/todo_app/domain/entities/user_task_entity.dart';

abstract class RandomCatPhrasesAndTasksStateService extends Equatable {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class Empty extends RandomCatPhrasesAndTasksStateService {}

class Loading extends RandomCatPhrasesAndTasksStateService {}

class Loaded extends RandomCatPhrasesAndTasksStateService {
  final UserTasksDomainEntity userTasksEntity;

  Loaded({required this.userTasksEntity});
}

class Error extends RandomCatPhrasesAndTasksStateService {
  final String message;

  Error({required this.message}) : super();

  @override
  List<Object> get props => [message];
}
