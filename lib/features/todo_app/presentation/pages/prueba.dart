import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/providers/web_service_api_provider.dart';
import 'package:provider/provider.dart';

class Prueba extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final p = Provider.of<WebServiceApiProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Prueba'),
      ),
      body: Column(
        children: [
          ListView.builder(
            itemCount: p.getGlobalListUpdateProvider.length,
            itemBuilder: (context, i) {
              return ListTile();
            },
          )
        ],
      ),
    );
  }
}
