import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/screens/home/home_todo_screen.dart';

class TodoAppPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: HomeTodoScreen(),
    );
  }
}
