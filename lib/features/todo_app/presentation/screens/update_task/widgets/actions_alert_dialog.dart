import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/screens/home/home_todo_screen.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/widgets_style_constants/textstyle/widget_text_style.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/widgets/styled_widgets/buttons/text_button_widget.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/widgets/styled_widgets/texts/text_widget.dart';

class ActionsAlertDialog extends StatelessWidget {
  const ActionsAlertDialog({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final TextEditingController? controller;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        TextButtonWidget(
          child: TextWidget(
            text: 'Abandonar',
            style: textStyle6,
          ),
          onPressed: () async {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => HomeTodoScreen(),
              ),
            );
            controller!.clear();
          },
        ),
        Spacer(),
        TextButtonWidget(
          child: TextWidget(
            text: 'Edit Task',
            style: textStyle6,
          ),
          onPressed: () async {
            Navigator.pop(context);
            controller!.clear();
          },
        ),
      ],
    );
  }
}
