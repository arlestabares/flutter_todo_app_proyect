import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/providers/web_service_api_provider.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/screens/new_taks/widgets/textbutton_showdialog.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/constants/constanst.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/widgets_style_constants/container/container_style.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/widgets_style_constants/textField/text_field_style.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/widgets_style_constants/textstyle/widget_text_style.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/widgets/styled_widgets/texts/text_widget.dart';
import 'package:provider/provider.dart';

class NewTaskScreen extends StatefulWidget {
  @override
  _NewTaskScreenState createState() => _NewTaskScreenState();
}

class _NewTaskScreenState extends State<NewTaskScreen> {
  String? message;
  String? cadena;
  TextEditingController? controller;

  @override
  void initState() {
    controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    controller!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var listProvider = Provider.of<WebServiceApiProvider>(context);
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: kPrimaryColor,
        title: TextWidget(text: 'New Task', style: textStyle3),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        height: size.height,
        child: Stack(
          children: [
            Container(
              height: 30,
              color: kPrimaryColor,
            ),
            Positioned(
              bottom: 0.0,
              child: Container(
                height: 70,
                width: size.width,
                color: Colors.black.withOpacity(0.8),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 30),
              decoration: boxDecorationRadiusCircularStyle1,
              width: size.width,
              height: size.height * 0.65,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: 25),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        TextWidget(text: "Book Tasks", style: textStyle2)
                      ],
                    ),
                    SizedBox(height: 30.0),
                    Container(
                      padding: EdgeInsets.all(15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextWidget(
                            text: 'Task Description',
                            style: textStyle2,
                          ),
                          SizedBox(height: 10),
                          Container(
                            height: 150.0,
                            width: double.infinity,
                            decoration: boxDecorationRadiusCircularStyle3,
                            child: TextField(
                              controller: controller,
                              onChanged: (value) {
                                cadena = value;
                                setState(() {});
                              },
                              maxLines: 6,
                              decoration: inputDecorationTextField,
                              style: textStyle2,
                            ),
                          ),
                          Container(
                            height: 50.0,
                            width: double.infinity,
                            decoration:
                                boxDecorationRadiusCircularWithOpacityStyle1,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  child: IconButton(
                                    onPressed: () {},
                                    icon: Icon(
                                      Icons.attach_file,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 70.0),
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 5.0),
                            width: double.infinity,
                            decoration: boxDecorationRadiusCircularStyle2,
                            child: Center(
                              child: TextButtonShowDialog(
                                cadena: cadena,
                                controller: controller,
                                listProvider: listProvider,
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
