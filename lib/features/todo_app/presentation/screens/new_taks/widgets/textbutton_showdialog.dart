import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/providers/web_service_api_provider.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/screens/home/home_todo_screen.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/widgets_style_constants/textstyle/widget_text_style.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/widgets/styled_widgets/buttons/text_button_widget.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/widgets/styled_widgets/texts/text_widget.dart';

class TextButtonShowDialog extends StatelessWidget {
  const TextButtonShowDialog({
    Key? key,
    required this.cadena,
    required this.controller,
    required this.listProvider,
  }) : super(key: key);

  final String? cadena;
  final TextEditingController? controller;
  final WebServiceApiProvider listProvider;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: TextWidget(text: 'Add Task', style: textStyle6),
      onPressed: () {
        if (cadena == null) {
          //
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                title: TextWidget(
                  text: 'El registro esta vacio',
                  style: textStyle3,
                ),
                actions: [
                  Row(
                    children: [
                      TextButtonWidget(
                        child: TextWidget(
                          text: 'Abandonar',
                          style: textStyle6,
                        ),
                        onPressed: () async {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => HomeTodoScreen(),
                            ),
                          );
                        },
                      ),
                      Spacer(),
                      TextButtonWidget(
                        child: TextWidget(text: 'Asignar', style: textStyle6),
                        onPressed: () async {
                          Navigator.pop(context);
                          controller!.clear();
                        },
                      ),
                    ],
                  )
                ],
              );
            },
          );
          //
        } else {
          Navigator.pop(context);
          listProvider.addItem(cadena!);
        }
        // setState(() {});
      },
    );
  }
}
