import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/providers/web_service_api_provider.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/screens/update_task/update_task_screen.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/constants/constanst.dart';
import 'package:provider/provider.dart';
import 'package:roundcheckbox/roundcheckbox.dart';

class RandomPhrasesPrint extends StatefulWidget {
  @override
  _RandomPhrasesPrintState createState() => _RandomPhrasesPrintState();
}

class _RandomPhrasesPrintState extends State<RandomPhrasesPrint> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    var listprovider = Provider.of<WebServiceApiProvider>(context);

    return Expanded(
      child: ListView.builder(
        itemCount: listprovider.getGlobalListUpdateProvider.length,
        itemBuilder: (context, i) {
          return Dismissible(
            background: Container(
              color: kPrimaryColor,
              child: Icon(Icons.cancel),
            ),
            secondaryBackground: Container(
              color: kPrimaryColor,
              child: Icon(Icons.cancel),
            ),
            direction: DismissDirection.horizontal,
            key: Key(listprovider.getGlobalListUpdateProvider[i]),
            onDismissed: (DismissDirection direction) async {
              listprovider.getGlobalListUpdateProvider.removeAt(i);
              listprovider.getGlobalListUpdateProvider;
              setState(() {});
            },
            child: InkWell(
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => UpdateTaskScreen(
                    position: i,
                    task: listprovider.getGlobalListUpdateProvider[i],
                  ),
                ),
              ),
              child: Card(
                elevation: 4.0,
                margin: EdgeInsets.all(8.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                child: ListTile(
                  title: Text('${listprovider.getGlobalListUpdateProvider[i]}'),
                  leading: RoundCheckBox(
                    onTap: (selected) => this.selected,
                    uncheckedWidget: Icon(Icons.close),
                    isChecked: true,
                    checkedColor: kPrimaryColor,
                    size: 25,
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
