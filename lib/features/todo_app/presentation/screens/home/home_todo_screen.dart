import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/providers/web_service_api_provider.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/screens/home/widgets/random_phrases_print.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/screens/new_taks/new_task_screen.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/searchtask/search_tasks.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/constants/constanst.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/widgets_style_constants/textstyle/widget_text_style.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/widgets/styled_widgets/buttons/icon_button_widget.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/widgets/styled_widgets/buttons/text_button_widget.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/widgets/styled_widgets/dialogs/flutter_toast_widget.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/widgets/styled_widgets/texts/text_widget.dart';
import 'package:provider/provider.dart';

class HomeTodoScreen extends StatefulWidget {
  @override
  _HomeTodoScreenState createState() => _HomeTodoScreenState();
}

class _HomeTodoScreenState extends State<HomeTodoScreen> {
  var number;
  dynamic stringValue;
  TextEditingController? controller;
  @override
  void initState() {
    controller = TextEditingController();
    stringValue = controller!.text;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    controller!.dispose();
  }

  bool get isInt => this is int;
  bool get isString => this is String;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Tasks List'),
        centerTitle: true,
        actions: [
          IconButtonWidget(
            iconSize: 30,
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(
                context: context,
                delegate: SearchTasks(),
              );
            },
          ),
          IconButtonWidget(
            iconSize: 34.0,
            icon: Icon(Icons.add),
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => NewTaskScreen(),
              ),
            ),
          ),
          IconButtonWidget(
            // iconSize: 30.0,
            icon: Icon(Icons.delete_forever),
            onPressed: () {
              setState(() {});
              Provider.of<WebServiceApiProvider>(context, listen: false)
                  .removeList();
            },
          )
        ],
      ),
      body: Column(
        children: [
          RandomPhrasesPrint(),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 20.0),
        child: FloatingActionButton.extended(
          backgroundColor: kPrimaryColor,
          label: TextWidget(
            text: 'Add Random Cats Phrases',
            style: textStyle1,
          ),
          onPressed: () {
            showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16),
                  ),
                  title:
                      TextWidget(text: 'Ingrese numero de frases aleatorias'),
                  content: TextField(
                    controller: controller,
                    onChanged: (String value) {
                      stringValue = value;

                      number = int.tryParse(stringValue);
                      print('stringValue =  $stringValue');
                    },
                  ),
                  actions: [
                    Center(
                      child: TextButtonWidget(
                        child: TextWidget(text: 'Add', style: textStyle4),
                        onPressed: () async {
                          print('EL valor de value = $stringValue ');
                          if (stringValue!.isEmpty) {
                            flutterToastWidget('Ingrese un Valor Numerico');
                            print('El valor es nulo');
                          } else if (number is int) {
                            Navigator.pop(context);
                            Provider.of<WebServiceApiProvider>(context,
                                    listen: false)
                                .getRandomPhrasesFromUrl(number);

                            print('runtimeType.........');
                            controller!.clear();
                            number = null;
                          } else {
                            flutterToastWidget('Ingrese un valor Numerico');
                            controller!.clear();
                          }
                        },
                      ),
                    ),
                  ],
                );
              },
            );
          },
        ),
      ),
    );
  }
}




// ignore: must_be_immutable
