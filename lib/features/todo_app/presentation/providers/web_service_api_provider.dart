import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_todo_app/core/app/input_converter.dart';
import 'package:flutter_todo_app/core/errors/failure.dart';
import 'package:flutter_todo_app/features/todo_app/data/dto/random_phrases_dto.dart';
import 'package:flutter_todo_app/features/todo_app/domain/usecases/get_random_cat_phrase_usecase.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/services/random_cat_phrases_state_service.dart';
import 'package:http/http.dart' as http;

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The nuumber must be a positive integer or zero';

class WebServiceApiProvider extends ChangeNotifier {
//

  GetRandomCatPhraseUseCase? getRandomCatPhraseDomainUseCase;
  InputConverter? inputConverter;

// usecase.call(7);
  // int? _limit = 0;
  List _globalListProvider = [];
  List<dynamic> randomCatResponseList = [];

  // int get mylimit => _limit!;

  List get getGlobalListUpdateProvider => _globalListProvider;

  // set mylimit(int limite) {
  //   this._limit = limite;
  //   print('mylimit ====== $_limit');
  //   notifyListeners();
  // }

  bool bandera = false;

  void removeItemAt(int index) {
    _globalListProvider.removeAt(index);
    notifyListeners();
  }

  void removeList() {
    _globalListProvider = [];
    notifyListeners();
  }

  void addItem(String message) {
    _globalListProvider.add(message);
    notifyListeners();
  }

  void update(String message, int postion) {
    _globalListProvider[postion] = message;
    notifyListeners();
  }

  void searchTasks(String task, int position) {
    _globalListProvider[position] = task;
  }

  void updateGlobalListProvider() {
    notifyListeners();
  }

  void getRandomPhrasesFromUrl(int limit) async {
    //https://catfact.ninja/facts?limit=1&max_length=100
    try {
      if (limit != 0) {
        String url = 'https://catfact.ninja/facts?limit=$limit&max_length=140';
        final response = await http.get(Uri.parse(url));
        print(response.body);
        Map dataMap = json.decode(response.body);
        randomCatResponseList = dataMap['data'];
        var catslist = [];
        randomCatResponseList.forEach((element) {
          catslist.add(element['fact']);
        });
        //Agregamos a la lista
        this._globalListProvider.addAll(catslist);
        //notificamos los cambios a los widgets subscritos.
        notifyListeners();
      } else {
        print('valor de limite no valido');
      }
    } catch (e) {
      throw Exception('Ha ocurrido un error cargando la data......');
    }
  }

  void getRandomCatPhrasesUseCase(String numberString) async {
    var inputEither = inputConverter!.stringToUnsignedInteger(numberString);
    if (!bandera) {
      return inputEither
          .fold((failure) => Error(message: INVALID_INPUT_FAILURE_MESSAGE),
              //
              (integer) async {
        Loading();
        final failureOrRandom =
            await getRandomCatPhraseDomainUseCase!(Params(limit: integer));
        return failureOrRandom.fold(
            (failure) => Error(message: _mapFailureOrMessage(failure)),
            (datarandom) => Loaded(userTasksEntity: datarandom!));
      });
    }
    bandera = false;
  }

  // List<dynamic> r = [];

  Future<RandomCatPhrasesDTO> getRandomCatPhrases(int limit) async {
    // https://catfact.ninja/facts?limit=1&max_length=100
    if (limit != 0 && !bandera) {
      String url = 'https://catfact.ninja/facts?limit=$limit&max_length=140';
      final response = await http.get(Uri.parse(url));
      bandera = true;
      return RandomCatPhrasesDTO.fromJson(jsonDecode(response.body));
    } else {
      return throw Exception('Ha ocurrido un error cargando la data');
    }
  }
}

String _mapFailureOrMessage(IFailure failure) {
  switch (failure.runtimeType) {
    case ServerFailure:
      return SERVER_FAILURE_MESSAGE;
    case CacheFailure:
      return CACHE_FAILURE_MESSAGE;
    default:
      return 'Unexpected Error';
  }
}
