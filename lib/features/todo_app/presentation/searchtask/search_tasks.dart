import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/providers/web_service_api_provider.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/screens/update_task/update_task_screen.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/constants/constanst.dart';
import 'package:provider/provider.dart';
import 'package:roundcheckbox/roundcheckbox.dart';

class SearchTasks extends SearchDelegate {
  final String seleccion = '';

  @override
  List<Widget> buildActions(BuildContext context) {
    //Acciones  del AppBar
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          //query para borrar las cadenas de texto ingresado por
          //la persona en la barra de busqueda
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
        Provider.of<WebServiceApiProvider>(context, listen: false)
            .updateGlobalListProvider();
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    //Instrucion que crea los resultados que vamos a mostra
    return Center(
      child: CircularProgressIndicator(
        color: kPrimaryColor,
        semanticsLabel: 'There is not Data',
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    //Las builSuggestions() Son las sugerencias que aparecen cuando la persona escribe.
    var listap = Provider.of<WebServiceApiProvider>(context, listen: false);
    if (query.isEmpty) {
      return Container();
    }

    final suggestion = listap.getGlobalListUpdateProvider
        .where((task) => task.toLowerCase().contains(query.toLowerCase()));

    bool selected = false;
    return ListView.builder(
      itemCount: suggestion.length,
      itemBuilder: (context, i) {
        return (listap.getGlobalListUpdateProvider.isNotEmpty)
            ? Dismissible(
                background: Container(
                  color: kPrimaryColor,
                  child: Icon(Icons.cancel),
                ),
                secondaryBackground: Container(
                  color: kPrimaryColor,
                  child: Icon(Icons.cancel),
                ),
                direction: DismissDirection.horizontal,
                key: Key(suggestion.elementAt(i)),
                onDismissed: (DismissDirection direction) async {
                  listap.getGlobalListUpdateProvider.removeAt(i);
                },
                child: InkWell(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => UpdateTaskScreen(
                        position: i,
                        task: suggestion.elementAt(i),
                      ),
                    ),
                  ),
                  child: Card(
                    elevation: 4.0,
                    margin: EdgeInsets.all(8.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16)),
                    child: ListTile(
                      title: Text('${suggestion.elementAt(i)}'),
                      leading: RoundCheckBox(
                        onTap: (selected) => selected,
                        uncheckedWidget: Icon(Icons.close),
                        isChecked: true,
                        checkedColor: Color(0xFFF96060),
                        size: 25,
                      ),
                    ),
                  ),
                ),
              )
            : Center(
                child: CircularProgressIndicator(),
              );
      },
    );
  }
}
