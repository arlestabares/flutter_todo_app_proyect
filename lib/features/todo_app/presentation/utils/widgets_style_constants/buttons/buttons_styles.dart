import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/constants/constanst.dart';
import 'package:fluttertoast/fluttertoast.dart';

final ButtonStyle buttonStyle1 = TextButton.styleFrom(
  primary: colorButton,
  shadowColor: Colors.grey,
  elevation: 3.0,
  textStyle: TextStyle(
    color: Colors.transparent,
    fontSize: 18.0,
    fontStyle: FontStyle.normal,
  ),
  side: BorderSide(color: Colors.grey),
  // minimumSize: Size(88, 40),
  // padding: EdgeInsets.symmetric(horizontal: 16.0),
);

final ButtonStyle buttonStyle2 = TextButton.styleFrom(
  primary: colorButton,
  textStyle: TextStyle(
    color: Colors.black.withOpacity(0.6),
    fontSize: 18.0,
    fontStyle: FontStyle.normal,
  ),
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(20.0),
  ),
);

final ButtonStyle buttonStyle3 = TextButton.styleFrom(
  primary: kPrimaryColor,
  textStyle: TextStyle(
    color: Colors.black.withOpacity(0.6),
    fontSize: 18.0,
    fontStyle: FontStyle.normal,
  ),
  shape: BeveledRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(5),
    ),
  ),
);
final fluttertoast = Fluttertoast.showToast(
  msg: "Debe Ingresar un Numero",
  toastLength: Toast.LENGTH_LONG,
  gravity: ToastGravity.CENTER,
  timeInSecForIosWeb: 1,
  backgroundColor: Colors.red,
  textColor: Colors.white,
  fontSize: 16.0,
);
