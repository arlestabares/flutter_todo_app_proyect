import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/constants/constanst.dart';

final textStyle1 = TextStyle(fontSize: 16);
final textStyle2 = TextStyle(fontSize: 18);
final textStyle3 = TextStyle(fontSize: 25);
final textStyle4 = TextStyle(fontSize: 18, color: kPrimaryColor);

final textStyle5 = TextStyle(color: kTitleTextDarkColor, fontSize: 16.0);
final textStyle6 =
    TextStyle(fontSize: 18.0, color: Colors.black.withOpacity(0.6));

final androidTextButtonTextStyle =
    TextStyle(fontSize: 18.0, color: kPrimaryColor);
//
final cupertinoTextButtonTextStyle =
    TextStyle(fontSize: 18.0, color: kPrimaryColor);
//
final textFieldTextStyle =
    TextStyle(fontSize: 18.0, color: Colors.black.withOpacity(0.6));

final alertDialogTextStyle =
    TextStyle(fontSize: 18.0, color: Colors.black.withOpacity(0.6));
