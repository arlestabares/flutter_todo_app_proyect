import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/utils/constants/constanst.dart';

final boxDecorationRadiusCircularStyle1 = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.all(
    Radius.circular(7.0),
  ),
);

final boxDecorationRadiusCircularStyle2 = BoxDecoration(
  borderRadius: BorderRadius.all(
    Radius.circular(15),
  ),
  color: kPrimaryColor,
);

final boxDecorationRadiusCircularStyle3 = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.only(
    topRight: Radius.circular(15.0),
    topLeft: Radius.circular(15.0),
  ),
  border: Border.all(
    color: Colors.grey.withOpacity(0.6),
  ),
);

final boxDecorationRadiusCircularWithOpacityStyle1 = BoxDecoration(
  color: Colors.grey.withOpacity(0.2),
  borderRadius: BorderRadius.only(
    bottomRight: Radius.circular(15.0),
    bottomLeft: Radius.circular(15.0),
  ),
  border: Border.all(
    color: Colors.grey.withOpacity(0.5),
  ),
);

final boxDecorationRadiusCircularWithOpacityStyle = BoxDecoration(
  color: Colors.grey.withOpacity(0.2),
  borderRadius: BorderRadius.only(
    bottomRight: Radius.circular(15.0),
    bottomLeft: Radius.circular(15.0),
  ),
  border: Border.all(
    color: Colors.grey.withOpacity(0.5),
  ),
);
