import 'package:flutter/material.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/pages/todo_app_page.dart';
import 'package:flutter_todo_app/features/todo_app/presentation/screens/home/home_todo_screen.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'todoScreenPage': (_) => HomeTodoScreen(),
  'todoAppPage': (_) => TodoAppPage()
};
