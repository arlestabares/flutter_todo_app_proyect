import 'package:dartz/dartz.dart';
import 'package:flutter_todo_app/core/errors/failure.dart';

class InputConverter {
//Cadena a entero sin signo
  Either<IFailure, int> stringToUnsignedInteger(String str) {
    try {
      if (str.isEmpty) throw FormatException();
      final integer = int.parse(str);
      if (integer < 0) throw FormatException();

      return Right(integer);
    } on FormatException {
      return Left(InvalidInputFailure());
    }
  }
}

class InvalidInputFailure extends IFailure {}
