import 'package:equatable/equatable.dart';

abstract class IUserTaskUseCase<Type, Params> {
  Future<void> call(Params params);
}

class NoParams extends Equatable {
  @override
  List<Object?> get props => [];
}
