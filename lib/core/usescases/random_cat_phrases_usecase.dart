import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_todo_app/core/errors/failure.dart';

abstract class IRandomCatPhrasesUseCase<Type, Params> {
  Future<Either<IFailure, Type>> call(Params params);
}

class NoParams extends Equatable {
  @override
  List<Object?> get props => [];
}
