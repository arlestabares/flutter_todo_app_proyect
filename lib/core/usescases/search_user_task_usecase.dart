import 'package:equatable/equatable.dart';

abstract class ISearchUserTaskUseCase<Type, Params> {
  Stream<List<Type>> call(Params params);
}

class NoParams extends Equatable {
  @override
  List<Object?> get props => [];
}
